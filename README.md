# Lure × Warde × Paged.js (static version)

Cette édition a été conçue pour les 72<sup>es</sup> Rencontres internationales de Lure, lors d’un atelier dirigé par Julie Blanc à Césure (13, rue de Santeuil, 75005 Paris), les 24 et 25 juin 2024.

Elle est composée en HTML et CSS avec [Paged.js](https://pagedjs.org/), une librairie JavaScript libre et *open source* implémentant les spécifications CSS pour l’impression. 

Ont participé à la conception graphique et au développement: Laura Amazo, Carole Appert, Nicolas Balaine, Julie Blanc, Inès Boucheikha, Panni Demeter, Maryl Genc, Anna George Lopez, Thibéry Maillard, Mia Thibierge.

Édition imprimée par l’atelier Quintal et distribué en 300 exemplaire lors des 72<sup>es</sup> Rencontres internationales de Lure (2024).

Le lancement du projet nécessite un serveur local. 
Attention, le code ne marche pas avec Firefox, merci d’utiliser Chromium ou Chrome.


## Sources

### Padatrad

L’environnement de développement qui a été utilisé <br>lors de l’atelier est une adaptation de [Padatrad](https://gitlab.com/editionsburnaout/padatrad) by Yann Trividic, développé par&nbsp;Yann Trividic et adapté par Julie Blanc. Cette version est statique et non basée sur des pad pour le HTML et le CSS.

Écrire directement les textes dans `index.html` et les styles dans  `style-print.css`

### Paged.js

Paged.js v0.4.3


### Textes 

Béatrice Warde, « Le Verre en cristal ou la typographie devrait être invisible », trad. de Stéphane Darricau; dans *Le graphisme en textes. Lectures indispensables*, Pyramyd, 2011, pp. 39-43.
 
Vanina Pinter, « Dispar(être) - le plomb de la transparence », publié sur *regarderparlafenetre.fr*, juillet 2018, <br>URL: https://regarderparlafenetre.fr/en/disparetre-le-plomb-de-la-transparence. Révisé en juin 2024 spécialement pour cette publication.


### Images

This is a printing office: https://fr.wikipedia.org/wiki/Beatrice_Warde#/media/Fichier:This_is_a_Printing_Office.jpg


