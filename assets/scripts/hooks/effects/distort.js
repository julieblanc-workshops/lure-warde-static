class distort extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
        this.setIllus = new Set();
    }

    afterRendered(pages){
        let body = document.querySelector("body");
        chargerContenu('assets/scripts/hooks/effects/distort-text.html', 'body');

        // let cover = document.querySelector("#cover");
        // var div = document.createElement('div');
        // div.classList.add("container-svg");
        // div.style.width = '97mm';
        // div.style.height = '270mm'; 
        // div.style.position = 'absolute';
        // div.style.top = '0';
        // div.id = "blob";
        // div.innerHTML = ' <svg viewBox="0 0 200 200" style="width: 97mm; height: 270mm; transform : scale(2);"><path fill="var(--color-2)"  d="M37.2,-62.9C43.2,-53.9,39.8,-35.3,41.4,-21.8C43.1,-8.3,49.9,-0.1,53.5,11C57.1,22.1,57.5,36.1,52.7,50.7C47.9,65.2,37.9,80.3,25.8,80.3C13.7,80.3,-0.5,65.3,-16.7,59.4C-32.9,53.4,-51,56.6,-59.6,49.9C-68.3,43.1,-67.5,26.3,-64.2,12.6C-60.9,-1.1,-55.1,-11.8,-51.2,-24.6C-47.3,-37.4,-45.2,-52.3,-37,-60.3C-28.8,-68.3,-14.4,-69.5,0.6,-70.4C15.6,-71.3,31.1,-71.9,37.2,-62.9Z" transform="translate(100 100)" /></svg> '
        // cover.appendChild(div);


      
    }
}

Paged.registerHandlers(distort);


function chargerContenu(fichier, cible) {
    fetch(fichier)
        .then(response => response.text())
        .then(data => {
            // Crée un élément temporaire pour contenir le nouveau contenu
            const tempDiv = document.createElement('div');
            tempDiv.innerHTML = data;

            // Insère le contenu dans l'élément cible
            document.querySelector(cible).appendChild(tempDiv);
        })
        .catch(error => console.error('Erreur lors du chargement du fichier:', error));
}
