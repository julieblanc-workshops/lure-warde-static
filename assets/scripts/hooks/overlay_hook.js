/**
 * @file Pagedjs' hook that adds an overlay to pagedjs to display content that's not in the print.
 * @author Yann Trividic
 * @license GPLv3
 * @see https://gitlab.com/the-moral-of-the-xerox-vf
 *
 * based on Benoit Launay's forensic.js script
 * @see https://gitlab.coko.foundation/pagedjs/templaters/forensic
 */

import Overlay from "../utils/overlay.js";

class OverlayHook extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
    }
    beforeParsed(content) {
        console.log("overlay is working...");
        let overlay = new Overlay(document.body) ;
        overlay.init(); // returns a promise that we don't have to wait for
        
    }

    afterPageLayout(pageElement, page, breakToken){
        if(pageElement.id == "page-1"){
            // console.log(pageElement);
            interfaceEvents();
        }

        let nbr = page.id.replace('page-', '');
        let span = document.querySelector("#nrb-pages");
        span.innerHTML = nbr;
    }


    afterRendered(pages){
        let print = pages.querySelector("#button-print");
        print.dataset.ready = 'true';
    }
}
Paged.registerHandlers(OverlayHook);







 

function  interfaceEvents(){

    let body = document.getElementsByTagName("body")[0];

   
    // set a "unique" filename based on title element, in case several books are opened
    var fileTitle = document.getElementsByTagName("title")[0].text;

        /* BASELINE ---------------------------------------------------------------------------------------------------- 
        ----------------------------------------------------------------------------------------------------------------*/

        /* Set baseline onload */
        let baselineToggle = localStorage.getItem('baselineToggle' + fileTitle);
        let baselineButton = document.querySelector('#label-baseline-toggle');
        let baselineSize = localStorage.getItem('baselineSize' + fileTitle);
        let baselinePosition = localStorage.getItem('baselinePosition');
        let baselineSizeInput = document.querySelector('#size-baseline');
        let baselinePositionInput = document.querySelector('#position-baseline');

        if(baselineToggle == "no-baseline"){
            body.classList.add('no-baseline');
            baselineButton.innerHTML = "see";
        }else if(baselineToggle == "baseline"){
            body.classList.remove('no-baseline');
            document.querySelector("#baseline-toggle").checked = "checked";
            baselineButton.innerHTML = "hide";
        }else{
            body.classList.add('no-baseline');
            localStorage.setItem('baselineToggle' + fileTitle, 'no-baseline');
            baselineButton.innerHTML = "see";
        }

        /* Get baseline size and position on load*/
        if(baselineSize){
            baselineSizeInput.value = baselineSize;
            document.documentElement.style.setProperty('--pagedjs-baseline', baselineSize + 'px');
        }else{
            localStorage.setItem('baselineSize' + fileTitle, baselineSizeInput.value);
        }
        baselinePositionInput.addEventListener("input", (e) => {
        });
        if(baselinePosition){
            baselinePositionInput.value = baselinePosition;
            document.documentElement.style.setProperty('--pagedjs-baseline-position', baselinePosition + 'px');
        }else{
            localStorage.setItem('baselineSPosition', baselinePositionInput.value);
        }




        /* Toggle baseline */
        document.querySelector("#baseline-toggle").addEventListener("input", (e) => {
            if(e.target.checked){
                /* see baseline */
                body.classList.remove('no-baseline');
                localStorage.setItem('baselineToggle' + fileTitle, 'baseline');
                baselineButton.innerHTML = "hide";
            }else{
                /* hide baseline */
                body.classList.add('no-baseline');
                localStorage.setItem('baselineToggle' + fileTitle, 'no-baseline');
                baselineButton.innerHTML = "see";
            }
        });


        /* Change baseline size on input */
        document.querySelector("#size-baseline").addEventListener("input", (e) => {
            document.documentElement.style.setProperty('--pagedjs-baseline', e.target.value + 'px');
            localStorage.setItem('baselineSize'  + fileTitle, baselineSizeInput.value);
        });


        /* Change baseline position on input */
          document.querySelector("#position-baseline").addEventListener("input", (e) => {
            document.documentElement.style.setProperty('--pagedjs-baseline-position', e.target.value + 'px');
            localStorage.setItem('baselinePosition', baselinePositionInput.value);
        });


         /* MARGIN BOXES ---------------------------------------------------------------------------------------------------- 
        ----------------------------------------------------------------------------------------------------------------*/
        let marginButton = document.querySelector('#label-marginbox-toggle');

        body.classList.add('no-marginboxes');
        
        document.querySelector("#marginbox-toggle").addEventListener("input", (e) => {
            if(e.target.checked){
                /* see baseline */
                body.classList.remove('no-marginboxes');
                marginButton.innerHTML = "hide";
            }else{
                /* hide baseline */
                body.classList.add('no-marginboxes');
                marginButton.innerHTML = "see";
            }
        });


        /* Preview ---------------------------------------------------------------------------------------------------- 
        ----------------------------------------------------------------------------------------------------------------*/


        document.querySelector("#preview-toggle").addEventListener("input", (e) => {
            if(e.target.checked){
                /* preview mode */
                body.classList.add('interface-preview');
            }else{
                body.classList.remove('interface-preview');
            }
        });
    
}



