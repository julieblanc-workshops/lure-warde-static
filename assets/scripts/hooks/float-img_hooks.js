let cssFloatImg = `.float-img_right{
    float: right;
    shape-outside: padding-box;
  }
  .col-inside_2{
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-gap: 3mm;    
    height: auto!important;
  }`;
  
  class floatImg extends Paged.Handler {
    constructor(chunker, polisher, caller) {
      super(chunker, polisher, caller);
      this.selectorRight = new Set();
      this.selectorBottom = new Set();
      this.selectorTop = new Set();
      this.selectorMoveElem  = new Set();
      this.selectorColInside2 = new Set();
  
    }
  
    onDeclaration(declaration, dItem, dList, rule) {
      // Read customs properties
      if (declaration.property == "--x-float-img") {
        // get selector of the declaration (NOTE: need csstree.js)
        let selector = csstree.generate(rule.ruleNode.prelude);
        // Push selector in correct set 
        if (declaration.value.value.includes("right")) {
          this.selectorRight.add(selector);
        }else if(declaration.value.value.includes("bottom")) {
          this.selectorBottom.add(selector);
        }else if(declaration.value.value.includes("top")) {
          this.selectorTop.add(selector);
        }
      }
      if (declaration.property == "--x-col-inside") {
        let selector = csstree.generate(rule.ruleNode.prelude);
        if (declaration.value.value.includes("2")) {
          this.selectorColInside2.add(selector);
        }
      }
    
      if (declaration.property == "--x-move-elem") {  
        let selector = csstree.generate(rule.ruleNode.prelude);
          let value = declaration.value.value
          let doubleArray = [selector, value];
          this.selectorMoveElem.add(doubleArray);
          // problem: an element is added 78 times in the set
    }
  }
  
  
  
  
  
  
  
  
    afterParsed(parsed){
      addcss(cssFloatImg);
  
      // ADD data to move elements
        for (let item of this.selectorMoveElem) {
          let elem = parsed.querySelector(item[0]);
          if(elem){
            elem.dataset.moveImg = item[1];
          }
        }
  
       moveElem(parsed);
  
  
        // ADD pagedjs classes to elements
    
      for (let item of this.selectorRight) {
        let elems = parsed.querySelectorAll(item);
        for (let elem of elems) {
          elem.classList.add("float-img_right");
        }
      }
      for (let item of this.selectorBottom) {
        let elems = parsed.querySelectorAll(item);
        for (let elem of elems) {
          elem.classList.add("float-img_bottom");
        }
      }
      for (let item of this.selectorTop) {
        let elems = parsed.querySelectorAll(item);
        for (let elem of elems) {
          elem.classList.add("float-img_top");
        }
      }
      
      for (let item of this.selectorColInside2) {
        let elems = parsed.querySelectorAll(item);
        for (let elem of elems) {
          elem.classList.add("col-inside_2");
        }
      }
    }
  
  
    afterPageLayout(pageElement, page, breakToken){
          let floatTopPage = pageElement.querySelector(".float-img_top");
      let pageContent = pageElement.querySelector(".pagedjs_page_content");
      if(floatTopPage){
        pageContent.insertBefore(floatTopPage, pageContent.firstChild);
      }
  
      let floatBottomPage = pageElement.querySelector(".float-img_bottom");
      if(floatBottomPage){
        pageContent.insertBefore(floatBottomPage, pageContent.firstChild);
        floatBottomPage.style.position = "absolute";
        floatBottomPage.style.bottom = "0px";
      }
  
      }
  
  
  
  }
  Paged.registerHandlers(floatImg);
  
  
  function  moveElem(parsed){
    let elems = parsed.querySelectorAll('[data-move-img]');
    for (let elem of elems) {
      let n = parseInt(elem.getAttribute('data-move-img'));
      let newPlace
      if(n < 0){
        newPlace = elem.previousSibling;
        if (newPlace.nodeType !== Node.ELEMENT_NODE) {
          newPlace = newPlace.previousSibling
        }
        n = n*-1 - 1;
        for(let i = 0; i < n; i++){
          newPlace = newPlace.previousSibling
          if (newPlace.nodeType !== Node.ELEMENT_NODE) {
            newPlace = newPlace.previousSibling
          }
        }
      }else{
        newPlace = elem.nextSibling;
        if (newPlace.nodeType !== Node.ELEMENT_NODE) {
          newPlace = newPlace.nextSibling;
        }
        for(let i = 0; i < n; i++){
          newPlace = newPlace.nextSibling;
          if (newPlace.nodeType !== Node.ELEMENT_NODE) {
            newPlace = newPlace.nextSibling;
          }
        }
      }
  
      newPlace.parentNode.insertBefore(elem, newPlace);
  
     
  // do next = next.nextSibling;     while(next && next.nodeType !== 1);
  
  
    }
  
    
  }
  
  
  
  function addcss(css){
    var head = document.getElementsByTagName('head')[0];
    var s = document.createElement('style');
    s.setAttribute('type', 'text/css');
    if (s.styleSheet) {   // IE
        s.styleSheet.cssText = css;
    } else {// the world
        s.appendChild(document.createTextNode(css));
    }
    head.appendChild(s);
  }