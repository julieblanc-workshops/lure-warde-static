class illuTypo extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
        this.setIllus = new Set();
    }

    beforeParsed(content) {

        let containerIllu = content.querySelector("#container-illustrations")
        let illus = content.querySelectorAll(".illu");
        illus.forEach((item, index) => {
            this.setIllus.add(item);
        });
        containerIllu.style.display = "none";


    }

    afterPageLayout(pageElement, page, breakToken){
  
        if(pageElement.classList.contains("pagedjs_chapter_page") && !pageElement.classList.contains("pagedjs_chapter_first_page")){

            let firstIllu = this.setIllus.values().next().value;

            if (firstIllu) {
                let pageArea = pageElement.querySelector(".pagedjs_area .pagedjs_page_content > div");
            ;
                let content = firstIllu.innerHTML;
                let illuId = firstIllu.dataset.id;
              

               

                let container = document.createElement("div");
                container.classList.add("illu");
                container.id = illuId;
                container.innerHTML = content;
                pageArea.insertAdjacentElement("afterbegin", container);

                this.setIllus.delete(firstIllu);

            }



             
             

        }


    }
}

Paged.registerHandlers(illuTypo);
