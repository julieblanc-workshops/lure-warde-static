class illuTypo extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
        this.note1 = new Set();
    }

  
    afterPageLayout(pageElement, page, breakToken){
  
        if(pageElement.id == "page-18"){
            this.note1 = pageElement.querySelector(".pagedjs_footnote_inner_content .note");
        }

        if(pageElement.id == "page-19"){
        
            
            let container = pageElement.querySelector(".pagedjs_footnote_inner_content");
            container.prepend(this.note1);

            let noteArea = pageElement.querySelector(".pagedjs_footnote_area");

            noteArea.style.position = "relative";
            noteArea.style.top = "-23px";

            noteArea.style.setProperty('--pagedjs-footnotes-height', "186px");


        }

    }
}

Paged.registerHandlers(illuTypo);
